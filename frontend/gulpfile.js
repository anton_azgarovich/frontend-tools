'use strict';

var gulp = require('gulp');
var stylus = require('gulp-stylus');
var plumber = require('gulp-plumber');
var rimraf = require('rimraf');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var rename = require('gulp-rename');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;
var watch = require('gulp-watch');
var changed = require('gulp-changed');
var jade = require('gulp-jade');
var jadeGlobbing  = require('gulp-jade-globbing');
var flatten = require('gulp-flatten');
var sourcemaps = require('gulp-sourcemaps');
var filter = require('gulp-filter');
var autoprefixer = require('gulp-autoprefixer');
var data = require('gulp-data');
var fs = require('fs');

// path

var path = {}, folder = {};

// folder
folder.html = 'jade/';
folder.stylus = 'stylus/';
folder.css = 'css/';
folder.js = 'js/';
folder.img = 'img/';
folder.fonts = 'fonts/';

// assets
path.assets = './';

path.assets_html = path.assets + folder.html;
path.assets_stylus = path.assets + folder.stylus;
path.assets_js = path.assets + folder.js;
path.assets_img = path.assets + folder.img;
path.assets_fonts = path.assets + folder.fonts;

// build
path.build = '../dev/';
path.static = path.build + 'static/';

path.build_html = path.build;
path.static_css = path.static + folder.css;
path.static_js = path.static + folder.js;
path.static_img = path.static + folder.img;
path.static_fonts = path.static + folder.fonts;

// project
path.staticfiles = '../project/staticfiles/';
path.staticfiles_css = path.staticfiles + folder.css;
path.staticfiles_js = path.staticfiles + folder.js;
path.staticfiles_img = path.staticfiles + folder.img;
path.staticfiles_fonts = path.staticfiles + folder.fonts;


// server
gulp.task('browser-sync', function () {

  browserSync.init(['./dev/**/*.*', './blocks/**/*.*'], {
    server: {
      baseDir: path.build,
      index: 'home.html'
    },
    files: [path.static_css + '*.css', path.build + '*.html']
  });
});

// Reload all Browsers
gulp.task('bs-reload', function () {
  browserSync.reload();
});


gulp.task('jade', function () {

  return gulp.src(['./pages/*.jade', '!./pages/_*.jade'])
    .pipe(plumber())
    .pipe(data( function(file) {
      return JSON.parse(fs.readFileSync('./data/data.json'));
    } ))
    .pipe(jadeGlobbing())
    .pipe(jade({pretty: true}))
    .pipe(changed(path.build, {extension: '.html', hasChanged: changed.compareSha1Digest}))
    .pipe(gulp.dest(path.build))
    .pipe(reload({stream: true}));
});

gulp.task('stylus', function () {
  return gulp.src('app.styl')
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(stylus())
    .pipe(autoprefixer())
    .pipe(rename('style.css'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(path.static_css))
    .pipe(gulp.dest(path.staticfiles_css))
    .pipe(filter('**/*.css'))
    .pipe(reload({stream: true}));
});

gulp.task('js', function () {
  return gulp.src('./blocks/**/*.js')
    .pipe(flatten())
    .pipe(changed(path.static_js))
    .pipe(plumber())
    .pipe(gulp.dest(path.static_js))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('js-bower', function () {
  // write path for files js
  return gulp.src([
    './bower_components/jquery/dist/jquery.js',
    './bower_components/bxslider-4/dist/jquery.bxslider.js',
    './bower_components/chosen/chosen.jquery.min.js',
    './bower_components/nouislider/distribute/nouislider.js',
    './bower_components/jquery-selectric/public/jquery.selectric.js'
  ])
    .pipe(changed(path.static_js))
    .pipe(gulp.dest(path.static_js))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('image', function () {
  return gulp.src('./blocks/**/*.{jpg,png,svg,gif}')
    .pipe(flatten())
    .pipe(changed(path.static_img))
    .pipe(imagemin({
      optimizationLevel: 3,
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()],
      interlaced: true
    }))
    .pipe(gulp.dest(path.static_img))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('fonts', function () {
  return gulp.src('./blocks/**/*.{woff,eot,ttf,otf}')
    .pipe(flatten())
    .pipe(changed(path.static_fonts))
    .pipe(gulp.dest(path.static_fonts))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('clean', function (cb) {
  rimraf(path.build, cb);
});


gulp.task('build', [
  'jade',
  'stylus',
  'js',
  'js-bower',
  'fonts',
  'image'
]);


gulp.task('watch-files', function () {
  watch(['./blocks/**/*.jade', './data/data.json'], function (event, cb) {
    gulp.start('jade');
  });
  watch(['./blocks/**/*.styl', 'app.styl'], function (event, cb) {
    gulp.start('stylus');
  });
  watch(['./blocks/static/js/**/*.js'], function (event, cb) {
    gulp.start('js');
  });
  watch(['./bower_components/**/*.js'], function (event, cb) {
    gulp.start('js-bower');
  });
  watch(['./blocks/**/*.{jpg,png,svg,gif}'], function (event, cb) {
    gulp.start('image');
  });
  watch(['./blocks/**/*.{oft,ttf,woff,eot}'], function (event, cb) {
    gulp.start('fonts');
  });
});

// default tasks

gulp.task('default', ['browser-sync', 'build', 'watch-files']);